package networkcall;

import com.google.gson.JsonElement;

import model.ItemData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {
    @GET("flanfiddich.json")
    Call<JsonElement> getData(@Query("auth") String auth);

    @POST("flanfiddich.json")
    Call<JsonElement> putData(@Body ItemData itemData);
}
