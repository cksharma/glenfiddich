package CustomAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.examp.glenfiddich.R;
import com.examp.glenfiddich.detail.DetailActivity;

import java.util.List;

import model.ItemData;

public class CustomListAdapter extends BaseAdapter {

    Activity useactivity;
    LayoutInflater inlator;
    List<ItemData> itemDataList;


    public CustomListAdapter(Activity activity, List<ItemData> itemDataList) {
        useactivity = activity;
        this.itemDataList=itemDataList;
        inlator = (LayoutInflater) useactivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return itemDataList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public void setSelectedPos(int pos) {
        notifyDataSetChanged();
    }

    public int getSelectedPos() {
        return 0;
    }

    public static class ViewHolder {
        Layout layout;
        TextView textviewName, textViewDesc, textViewprice;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder viewholder;
        if (convertView == null) {
            viewholder = new ViewHolder();
            convertView = inlator.inflate(R.layout.list_item_product, null);
            viewholder.textviewName = (TextView) convertView.findViewById(R.id.name);
            viewholder.textViewDesc= (TextView) convertView.findViewById(R.id.desc);
            viewholder.textViewprice = (TextView) convertView.findViewById(R.id.price);
            convertView.setTag(viewholder);
           /* if (cnt == 0) {
                cnt = 1;
                convertView.setBackgroundColor(Color.rgb(255, 255, 255));
            } else {
                cnt = 0;
                convertView.setBackgroundColor(Color.rgb(255, 255, 255));
            }*/
        } else
            viewholder = (ViewHolder) convertView.getTag();

        viewholder.textviewName.setText(itemDataList.get(position).getName());
        viewholder.textViewDesc.setText(itemDataList.get(position).getDesc());
        viewholder.textViewprice.setText("Price: \u20B9"+" "+itemDataList.get(position).getPrice());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(useactivity, DetailActivity.class);
                intent.putExtra("data",itemDataList.get(position));
                useactivity.startActivity(intent);
            }
        });
        return convertView;
    }

}
