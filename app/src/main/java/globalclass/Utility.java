package globalclass;

import android.content.Context;
import android.net.ConnectivityManager;

public class Utility {
    public static boolean _f_chk_internet_conn(Context c) {
        final ConnectivityManager conMgr = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected())
            return true;
        else
            //System.out.println("Internet Connection Not Present");
            return false;
    }

}
