package com.examp.glenfiddich.home;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import CustomAdapter.CustomListAdapter;
import globalclass.Constant;
import globalclass.Utility;
import model.ItemData;
import networkcall.ApiClient;
import networkcall.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter implements MainView.PresenterView{

    Activity context;
    MainView.View  view;
    public MainPresenter(Activity context,MainView.View view) {
        this.context=context;
        this.view=view;
    }

    @Override
    public void getData(){
        final List<ItemData>  itemDataList=new ArrayList<>();
        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);

        Call<JsonElement> call=apiService.getData(Constant.AUTH);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                System.out.println("outpot="+response.body());

                try {

                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    Iterator iterator=jsonObject.keys();

                    while (iterator.hasNext()){
                        ItemData itemData = new ItemData();

                        String key = (String) iterator.next();
                        itemData.setName(jsonObject.getJSONObject(key).optString(Constant.NAME));
                        itemData.setDesc(jsonObject.getJSONObject(key).optString(Constant.DESC));
                        itemData.setPrice(jsonObject.getJSONObject(key).optString(Constant.PRICE));

                        itemDataList.add(itemData);
                    }

                    CustomListAdapter customListAdapter = new CustomListAdapter(context,itemDataList);
                    view.setAdapter(customListAdapter);

                }catch (Exception e){
                    e.printStackTrace();
                }



            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });
    }

    @Override
    public void initListener() {

        view.initListener();
    }

    @Override
    public void init() {
        view.init();

    }

    @Override
    public void addValueListener(DatabaseReference reference) {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(Utility._f_chk_internet_conn(context)) {
                    getData();
                }else{
                    Toast.makeText(context, "Internet/wi-fi not available",
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
