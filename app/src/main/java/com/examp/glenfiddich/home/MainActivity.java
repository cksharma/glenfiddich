package com.examp.glenfiddich.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.examp.glenfiddich.AddDataActivity;
import com.examp.glenfiddich.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import CustomAdapter.CustomListAdapter;
import globalclass.Constant;
import globalclass.Utility;
import model.ItemData;
import networkcall.ApiClient;
import networkcall.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements MainView.View {

    DatabaseReference reference;

    ListView listView;
    List<ItemData> itemDataList;

    MainPresenter presenter;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        presenter=new MainPresenter(this,this);

        presenter.init();
        presenter.initListener();
    }

    @Override
    protected void onStart(){
        super.onStart();
        if(reference!=null)
        presenter.addValueListener(reference);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setAdapter(CustomListAdapter adapter) {

        if(adapter!=null){
            listView.setAdapter(adapter);
        }

    }

    @Override
    public void initListener() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/

                Intent intent=new Intent(MainActivity.this,AddDataActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public void init() {

         fab = (FloatingActionButton) findViewById(R.id.fab);
        listView=(ListView)findViewById(R.id.lvExp);

        itemDataList=new ArrayList<>();

        reference= FirebaseDatabase.getInstance().getReference(Constant.DATA_BASE);

    }
}
