package com.examp.glenfiddich.home;

import android.app.Activity;

import com.google.firebase.database.DatabaseReference;

import CustomAdapter.CustomListAdapter;

public interface MainView {

    public interface View{

        void setAdapter(CustomListAdapter adapter);
        void initListener();
        void init();

    }
    public interface PresenterView{

        void getData();
        void initListener();
        void init();
        void addValueListener(DatabaseReference reference);
    }

}
