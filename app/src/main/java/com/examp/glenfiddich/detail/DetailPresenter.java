package com.examp.glenfiddich.detail;

import model.ItemData;

/**
 * Created by AD on 10/17/2018.
 */

public class DetailPresenter implements DetailView.PresenterView{
    private ItemData itemData;
    private DetailView.View view;
    public DetailPresenter(DetailView.View view,ItemData itemData) {
        this.view = view;
        this.itemData = itemData;
    }

    @Override
    public void setData() {
        view.setData(itemData);
    }

    @Override
    public void initViews() {
        view.init();
    }

    @Override
    public void setupToolBar() {
        view.setupToolBar();
    }
}
