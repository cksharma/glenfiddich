package com.examp.glenfiddich.detail;

import model.ItemData;

/**
 * Created by AD on 10/17/2018.
 */

public interface DetailView {

    public interface View {
        void init();
        void setupToolBar();
        void setData(ItemData itemData);
    }

    public interface PresenterView {
        void setData();
        void initViews();
        void setupToolBar();
    }

}
