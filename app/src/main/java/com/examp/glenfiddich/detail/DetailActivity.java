package com.examp.glenfiddich.detail;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.examp.glenfiddich.R;

import model.ItemData;

public class DetailActivity extends AppCompatActivity implements DetailView.View{

    private DetailPresenter presenter;
    private ImageView imgProduct;
    private TextView txtProductName,txtPrice,txtDescription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if (getIntent().hasExtra("data")){
            ItemData itemData = getIntent().getParcelableExtra("data");
            presenter = new DetailPresenter(this,itemData);
            presenter.setupToolBar();
            presenter.initViews();
            presenter.setData();
        }else {
            Toast.makeText(this, "Something went wrong,Please try again.", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    @Override
    public void init() {
        txtProductName = (TextView) findViewById(R.id.txtProductName);
        txtPrice = (TextView) findViewById(R.id.txtPrice);
        txtDescription = (TextView) findViewById(R.id.txtDescription);
    }

    @Override
    public void setupToolBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setData(ItemData itemData) {
        txtProductName.setText(itemData.getName());
        txtPrice.setText(getResources().getString(R.string.rupees)+itemData.getPrice());
        txtDescription.setText(itemData.getDesc());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return true;
    }
}
