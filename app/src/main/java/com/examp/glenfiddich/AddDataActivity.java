package com.examp.glenfiddich;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.Iterator;

import CustomAdapter.CustomListAdapter;
import globalclass.Utility;
import model.ItemData;
import networkcall.ApiClient;
import networkcall.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDataActivity extends AppCompatActivity {

    EditText name,desc,price;
    Button btnsave;
    ItemData itemData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data);

        name=(EditText)findViewById(R.id.name);
        desc=(EditText)findViewById(R.id.desc);
        price=(EditText)findViewById(R.id.price);
        btnsave=(Button) findViewById(R.id.btn_save);

        itemData=new ItemData();

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(getItemData()){

                    if(Utility._f_chk_internet_conn(AddDataActivity.this)) {
                        putData();
                    }else{
                        Toast.makeText(AddDataActivity.this, "Internet/wi-fi not available",
                                Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

    }

    public void putData(){
        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);

        Call<JsonElement> call=apiService.putData(itemData);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                System.out.println("response="+response.body().toString());

                Toast.makeText(AddDataActivity.this,"Data Add successfully....",Toast.LENGTH_LONG).show();
                AddDataActivity.this.finish();
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });
    }


    private boolean getItemData() {

        if (!TextUtils.isEmpty(name.getText().toString())) {
            itemData.setName(name.getText().toString());
        }else{
            Toast.makeText(AddDataActivity.this,"Please Enter Name",Toast.LENGTH_LONG).show();
            return false;
        }
        if (!TextUtils.isEmpty(price.getText().toString())) {
            itemData.setPrice(price.getText().toString());
        }else{
            Toast.makeText(AddDataActivity.this,"Please Enter Price",Toast.LENGTH_LONG).show();
            return false;
        }
        if (!TextUtils.isEmpty(desc.getText().toString())){
            itemData.setDesc(desc.getText().toString());
    }else{
            Toast.makeText(AddDataActivity.this,"Please Enter Description",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
